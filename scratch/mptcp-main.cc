#include "ns3/string.h"
#include "ns3/tcp-socket-base.h"
//#include "ns3/mptcp-socket-base.h"
//#include "ns3/mptcp-subflow.h"
#include "ns3/mp-tcp-socket-base.h"
#include "ns3/mp-tcp-subflow.h"

#include "ns3/ipv4-end-point.h"
#include "ns3/arp-l3-protocol.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/ipv6-l3-protocol.h"
#include "ns3/icmpv4-l4-protocol.h"
#include "ns3/icmpv6-l4-protocol.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/tcp-l4-protocol.h"
#include "ns3/trace-helper.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/sequence-number.h"
#include "ns3/trace-helper.h"
//#include "ns3/tcp-trace-helper.h"
#include "ns3/internet-module.h"
#include "ns3/inet-socket-address.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/core-module.h"


#include <string>
#include <fstream>

#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"  
/* logging in tests is not *allowed*
   TODO Move this to the example folder rather
   */
NS_LOG_COMPONENT_DEFINE ("MpTcpMain");

using namespace ns3;
using std::cout;
using std::endl;

    Ptr<Node>
CreateInternetNode ()
{
    Ptr<Node> node = CreateObject<Node> ();
    //ARP
    Ptr<ArpL3Protocol> arp = CreateObject<ArpL3Protocol> ();
    node->AggregateObject (arp);
    //IPV4
    Ptr<Ipv4L3Protocol> ipv4 = CreateObject<Ipv4L3Protocol> ();
    //Routing for Ipv4
    Ptr<Ipv4ListRouting> ipv4Routing = CreateObject<Ipv4ListRouting> ();
    ipv4->SetRoutingProtocol (ipv4Routing);
    Ptr<Ipv4StaticRouting> ipv4staticRouting = CreateObject<Ipv4StaticRouting> ();
    ipv4Routing->AddRoutingProtocol (ipv4staticRouting, 0);
    node->AggregateObject (ipv4);
    //ICMP
    Ptr<Icmpv4L4Protocol> icmp = CreateObject<Icmpv4L4Protocol> ();
    node->AggregateObject (icmp);
    //UDP
    Ptr<UdpL4Protocol> udp = CreateObject<UdpL4Protocol> ();
    node->AggregateObject (udp);



    Ptr<TcpL4Protocol> tcp = CreateObject<TcpL4Protocol> ();
    node->AggregateObject (tcp);
    return node;
}



int main (int argc, char *argv[])
{
    LogComponentEnable ("MpTcpMain", LOG_LEVEL_ALL);
    std::string bufSize = "";
    bool disWifi = false;
    bool disLte = false;
    bool isDownlink = true;
    double stopTime = 20.0;
    std::string p2pdelay = "10ms";
    int m_numberOfSubflows = 2;

    Ipv4StaticRoutingHelper ipv4routinghelper;


    CommandLine cmd;
    cmd.AddValue ("bufsize", "Snd/Rcv buffer size.", bufSize);
    cmd.AddValue ("nSubFlow", "number of subflows.",m_numberOfSubflows );
    /*
       cmd.AddValue ("disWifi", "Disable WiFi.", disWifi);
       cmd.AddValue ("disLte", "Disable LTE.", disLte);
       cmd.AddValue ("isDownlink", "is Downlink flow or uplink flow.", isDownlink);
       cmd.AddValue ("nUE", "the number of UEs.", nUE);
       cmd.AddValue ("stopTime", "StopTime of simulatino.", stopTime);
       cmd.AddValue ("p2pDelay", "Delay of p2p links. default is 10ms.", p2pdelay);
       */
    cmd.Parse (argc, argv);

    //Config::SetDefault ("ns3::TcpSocketBase::EnableMpTcp", BooleanValue(true) );
      Config::SetDefault("ns3::TcpL4Protocol::SocketType", TypeIdValue(MpTcpSocketBase::GetTypeId()));
  Config::SetDefault("ns3::MpTcpSocketBase::MaxSubflows", UintegerValue(8)); // Sink

    //Packet::EnablePrinting();

    const char* netmask = "255.255.255.0";
    //  const char* ipaddr0 = "192.168.1.0";
    //  const char* ipaddr1 = "192.168.1.2";
    //

   // Ptr<Node> m_serverNode = CreateInternetNode ();
    //Ptr<Node> m_sourceNode = CreateInternetNode ();
  NodeContainer nodes;
  nodes.Create(2);
 Ptr<Node> m_serverNode = nodes.Get(0);
Ptr<Node> m_sourceNode = nodes.Get(1); 


  InternetStackHelper internet;
  internet.Install(nodes);
/*
    Ptr<FlowMonitor> flowMonitor;
    FlowMonitorHelper flowHelper;
    flowMonitor = flowHelper.InstallAll();
*/
    // Create one path per subflow
    for(int i = 0; i < m_numberOfSubflows; ++i)
    {

        std::stringstream netAddr;
        netAddr << "192.168." << i << ".0";

        PointToPointHelper p2p;
        p2p.SetDeviceAttribute ("DataRate", StringValue ("100Mbps"));
        p2p.SetChannelAttribute ("Delay", StringValue ("10ms"));
        NetDeviceContainer cont = p2p.Install(m_serverNode, m_sourceNode);
        p2p.EnablePcapAll("mptcp-tcp", true);

        Ipv4AddressHelper ipv4;
        NS_LOG_DEBUG("setting ipv4 base " << netAddr.str());
        ipv4.SetBase( netAddr.str().c_str(), netmask);
        Ipv4InterfaceContainer ipInterfs =  ipv4.Assign(cont);
      //  cout<<ipInterfs.GetAddress(0, 0)<<endl;

        /// Added by matt for debugging purposes

        PointToPointHelper helper;
        helper.EnablePcapAll("test",true);
        // helper.EnablePcapAll("testmptcp", false);

    }

    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    ApplicationContainer apps;
    OnOffHelper onoff = OnOffHelper ("ns3::TcpSocketFactory",
            InetSocketAddress ("192.168.0.1", 9));
    onoff.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
    onoff.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
    onoff.SetAttribute ("PacketSize", StringValue ("1024"));
    onoff.SetAttribute ("DataRate", StringValue ("10Mbps"));
    apps = onoff.Install (m_sourceNode);
    apps.Start (Seconds (4.0));

    PacketSinkHelper sink = PacketSinkHelper ("ns3::TcpSocketFactory",
            InetSocketAddress (Ipv4Address::GetAny(), 9));


    apps = sink.Install (m_serverNode);
    apps.Start (Seconds (3));


    /*
       uint16_t port = 50000;
       InetSocketAddress serverlocaladdr (Ipv4Address::GetAny (), port);
       InetSocketAddress serverremoteaddr ( m_serverNode->GetObject<Ipv4>()->GetAddress(1,0).GetLocal(), port);

       server->Bind (serverlocaladdr);
       server->Listen ();
       server->SetAcceptCallback (MakeNullCallback<bool, Ptr< Socket >, const Address &> (),
       MakeCallback (&MpTcpTestCase::ServerHandleConnectionCreated,this));

    //  NS_LOG_INFO( "test" << server);
    // TODO check Connect called only once
    source->SetConnectCallback(MakeCallback (&MpTcpTestCase::OnMetaConnectionSuccessful, this),
    MakeNullCallback<void, Ptr<Socket> >());
    source->SetRecvCallback (MakeCallback (&MpTcpTestCase::SourceHandleRecv, this));
    source->SetSendCallback (MakeCallback (&MpTcpTestCase::SourceHandleSend, this));

    source->Connect (serverremoteaddr);
    */
    Simulator::Stop (Seconds (stopTime));
    Simulator::Run ();
      Simulator::Destroy();
  NS_LOG_INFO ("Done.");

}


