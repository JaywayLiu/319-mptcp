/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Morteza Kheirkhah <m.kheirkhah@sussex.ac.uk>
 * revized by Jianwei Liu <ljw725@gmail.com>
 */
//
//      left lte nodes --- pgw -- p2p links ---  cns
//      7.0.0.2-7.0.0.n+1    10.1.0.routerI  10.2.*.2    10.2.*.1
//

#include <string>
#include <fstream>
#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/network-module.h"
#include "ns3/packet-sink.h"

#include "ns3/lte-module.h"
#include "ns3/wifi-module.h"

#include "ns3/mobility-module.h"
#include "ns3/constant-position-mobility-model.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("MpTcpBulkSendExample");

void setPos (Ptr<Node> n, int x, int y, int z)
{
  Ptr<ConstantPositionMobilityModel> loc = CreateObject<ConstantPositionMobilityModel> ();
  n->AggregateObject (loc);
  Vector locVec2 (x, y, z);
  loc->SetPosition (locVec2);
}



int
main(int argc, char *argv[])
{
  LogComponentEnable("MpTcpSocketBase", LOG_INFO);

    const char* netmask = "255.255.255.0";
     int nUE= 2;

     int stopTime = 10;

  std::ostringstream cmd_oss;

    CommandLine cmd;
  cmd.AddValue ("nUE", "the number of UEs.", nUE);



  Config::SetDefault("ns3::Ipv4GlobalRouting::FlowEcmpRouting", BooleanValue(true));
  Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(1400));
  Config::SetDefault("ns3::TcpSocket::DelAckCount", UintegerValue(0));
  Config::SetDefault("ns3::DropTailQueue::Mode", StringValue("QUEUE_MODE_PACKETS"));
  Config::SetDefault("ns3::DropTailQueue::MaxPackets", UintegerValue(100));
  Config::SetDefault("ns3::TcpL4Protocol::SocketType", TypeIdValue(MpTcpSocketBase::GetTypeId()));
  Config::SetDefault("ns3::MpTcpSocketBase::MaxSubflows", UintegerValue(8)); // Sink
  //Config::SetDefault("ns3::MpTcpSocketBase::CongestionControl", StringValue("RTT_Compensator"));
  //Config::SetDefault("ns3::MpTcpSocketBase::PathManagement", StringValue("NdiffPorts"));

 // phy;
  int routerI;

  Ipv4AddressHelper address1;

  address1.SetBase ("10.1.0.0", "255.255.255.0");
  Ipv4InterfaceContainer if1;

  //define node and devices
  NodeContainer mn, routers, cn;
  NetDeviceContainer wifiDevice;
  NetDeviceContainer devices1;



     MobilityHelper mobility;
   mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                  "MinX", DoubleValue (1.0),
                                  "MinY", DoubleValue (1.0),
                                  "DeltaX", DoubleValue (5.0),
                                  "DeltaY", DoubleValue (5.0),
                                  "GridWidth", UintegerValue (3),
                                  "LayoutType", StringValue ("RowFirst"));
   mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                              "Mode", StringValue ("Time"),
                              "Time", StringValue ("2s"),
                              "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"),
                              "Bounds", RectangleValue (Rectangle (0.0, 20.0, 0.0, 20.0)));


  mn.Create(nUE);
  //routers.Create (1);
  cn.Create(nUE);


   mobility.Install (mn);
     // setPos (routers.Get (0), -20, 10, 0);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute("DataRate", StringValue("100Mbps"));
  pointToPoint.SetChannelAttribute("Delay", StringValue("1ms"));

  NetDeviceContainer devices;
  //devices = pointToPoint.Install(mn);

  InternetStackHelper internet;
  internet.Install(mn);
  //internet.Install(routers);
  internet.Install(cn);


    Ipv4StaticRoutingHelper ipv4routinghelper;

Ptr<Ipv4StaticRouting> mnStaticRoutingForPrint = ipv4routinghelper.GetStaticRouting (mn.Get(0)->GetObject<Ipv4>());


 Ptr<OutputStreamWrapper> rtoutUE = Create<OutputStreamWrapper> ("wifi-only-rtableUE", std::ios::out);


 /*

  Ptr<Node> bridgeWifi = CreateObject<Node> ();
      // Left link: H1 <-> WiFi-R
      WifiHelper wifi = WifiHelper::Default ();
    wifi.SetRemoteStationManager ("ns3::AarfWifiManager");
      YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
      YansWifiChannelHelper phyChannel = YansWifiChannelHelper::Default ();
      NqosWifiMacHelper mac;
      phy.SetChannel (phyChannel.Create ());
      //mac.SetType ("ns3::AdhocWifiMac");
      //
      int WifiAPI = 0;

      cmd_oss.str ("");

      cmd_oss << "wifi-default-" << WifiAPI;
    Ssid ssid = Ssid (cmd_oss.str ());

  mac.SetType ("ns3::StaWifiMac", "Ssid", SsidValue (ssid),  "ActiveProbing", BooleanValue (false));
      
      wifi.SetStandard (WIFI_PHY_STANDARD_80211a);
       
      
      for(uint32_t u = 0; u < mn.GetN (); ++u){
       wifiDevice = wifi.Install (phy, mac, mn.Get (u));
      devices1.Add(wifiDevice);
       }


      routerI = mn.GetN();

       mac.SetType ("ns3::ApWifiMac", "Ssid", SsidValue (ssid));
       wifiDevice = wifi.Install (phy, mac,  routers.Get (0));

      devices1.Add(wifiDevice);
      // Assign ip addresses
      if1 = address1.Assign (devices1);

       for(uint32_t u = 0; u < if1.GetN (); ++u){
          cout<<"if1: " <<u <<" "<<if1.GetAddress(u, 0)<<endl; 
       }

     cout<<"routerI="<<routerI<<endl;
*/


  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
// Ipv4AddressHelper address2;

 // address2.SetBase ("10.2.0.0", "255.255.255.0");

    NodeContainer enbNodes;

      Ptr<Node> bridgeLte = CreateObject<Node> ();
      //node 13
      enbNodes.Create(1);

      lteHelper->SetEpcHelper (epcHelper);
      Ptr<Node> pgw = epcHelper->GetPgwNode ();
      //setPos (enbNodes.Get (0), 60, 40000, 0);
      setPos (enbNodes.Get (0), -20, 14, 0);

        //lteHelper->SetSchedulerType ("ns3::PssFfMacScheduler");
        lteHelper->SetSchedulerType ("ns3::PfFfMacScheduler");

      NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
      NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (mn);

      // Assign ip addresses
      if1 = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

      for (int ii=0; ii< if1.GetN(); ii++)
          cout<<"lte if1  " <<ii <<"  " <<if1.GetAddress (ii, 0)<<endl;

       for(uint32_t u = 0; u < mn.GetN (); ++u){
      lteHelper->Attach (ueLteDevs.Get(u), enbLteDevs.Get(0));
       }


  lteHelper->EnableTraces ();


      routerI = mn.GetN();

      // LTE-R <-> H2
      // Right link
      //devices2 = pointToPoint.Install (cn.Get (0), pgw);

          PointToPointHelper p2p;
        p2p.SetDeviceAttribute ("DataRate", StringValue ("100Mbps"));
        p2p.SetChannelAttribute ("Delay", StringValue ("10ms"));
    
        
        for(int i = 1; i <= nUE; ++i)
    {

        std::stringstream netAddr;
        netAddr << "10.2." << i << ".0";


        NetDeviceContainer cont = p2p.Install(cn.Get(i-1), pgw);
        p2p.EnablePcapAll(netAddr.str(), true);

        Ipv4AddressHelper ipv4;
        NS_LOG_DEBUG("setting ipv4 base " << netAddr.str());
        ipv4.SetBase( netAddr.str().c_str(), netmask);
        Ipv4InterfaceContainer ipInterfs =  ipv4.Assign(cont);
        cout<<ipInterfs.GetAddress(0, 0)<<"  ";
        cout<<ipInterfs.GetAddress(1, 0)<<endl;
    }

        /*

        for(uint32_t u = 0; u < mn.GetN () -1; ++u){
Ptr<Ipv4StaticRouting> mnStaticRouting = ipv4routinghelper.GetStaticRouting (mn.Get(u)->GetObject<Ipv4>());
      //mnStaticRouting ->AddNetworkRouteTo(if2Wifi.GetAddress (0, 0), Ipv4Mask ("255.255.255.0"), if1.GetAddress (routerI, 0), devices1.Get (u)->GetIfIndex ());
      mnStaticRouting ->SetDefaultRoute(if1.GetAddress (routerI, 0), devices1.Get (u)->GetIfIndex ());
        cout<<"devices1.Get (u)->GetIfIndex () : "<<devices1.Get (u)->GetIfIndex ()<<endl;
       }
       */
    
    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();




   mnStaticRoutingForPrint->PrintRoutingTable(rtoutUE);

  uint16_t port = 9;

  for (int i=0; i< nUE; i++)
  {
  MpTcpPacketSinkHelper sink("ns3::TcpSocketFactory", InetSocketAddress(Ipv4Address::GetAny(), port));
  ApplicationContainer sinkApps = sink.Install(mn.Get(i));
 sinkApps.Start(Seconds(0.1));
  sinkApps.Stop(Seconds(stopTime));

  }




  for (int i=0; i< nUE; i++)
  {
cmd_oss.str ("");
      cmd_oss << "7.0.0."<<i+2; 

  //MpTcpBulkSendHelper source("ns3::TcpSocketFactory", InetSocketAddress(Ipv4Address(i.GetAddress(1)), port));
  MpTcpBulkSendHelper source("ns3::TcpSocketFactory", InetSocketAddress(cmd_oss.str().c_str(), port));
  source.SetAttribute("MaxBytes", UintegerValue(0));
  ApplicationContainer sourceApps = source.Install(cn.Get(i));
 sourceApps.Start(Seconds(0.1));
  sourceApps.Stop(Seconds(stopTime));

  }

  //phy.EnablePcapAll ("wifi", false);
  
  NS_LOG_INFO ("Run Simulation.");
  Simulator::Stop(Seconds(stopTime));
  Simulator::Run();
  Simulator::Destroy();
  NS_LOG_INFO ("Done.");

}
