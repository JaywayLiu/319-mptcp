/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or mod it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; , write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Morteza Kheirkhah <m.kheirkhah@sussex.ac.uk>
 * revized by Jianwei Liu <ljw725@gmail.com>
 */
//
//      left wdes --- wter -- p2p mn ---cns
//      10.1.0.*    10.1.0.routerI  10.2.*.2    10.2.*.1
//  * starts from 1
//

#include <string>
#include <fstream>
#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/network-module.h"
#include "ns3/packet-sink.h"

#include "ns3/lte-module.h"
#include "ns3/wifi-module.h"

#include "ns3/mobility-module.h"
#include "ns3/constant-position-mobility-model.h"

using namespace ns3;
using std::cout;
using std::endl;

NS_LOG_COMPONENT_DEFINE("MpTcpBulkSendExample");

//#define LTE
#define WIFI


void setPos(Ptr<Node> n, int x, int y, int z)
{
    Ptr<ConstantPositionMobilityModel> loc = CreateObject<ConstantPositionMobilityModel>();
    n->AggregateObject(loc);
    Vector locVec2(x, y, z);
    loc->SetPosition(locVec2);
}

///////////////////////////////////////////////
unsigned long int m_bytesTotal[5];
const int INTERVAL = 5;


void setPosSpeed (Ptr<Node> n, int x, int y, int z, const Vector& speed)
{
    //Ptr<ConstantPositionMobilityModel> loc = CreateObject<ConstantPositionMobilityModel> ();

    Ptr<ConstantVelocityMobilityModel> loc = CreateObject<ConstantVelocityMobilityModel> ();
    n->AggregateObject (loc);
    Vector locVec2 (x, y, z);
    loc->SetPosition (locVec2);

    loc->SetVelocity (speed);
}

void ReceivedPacket(std::string context, Ptr<const Packet> p, const Address &addr)
{
    m_bytesTotal[0] += p->GetSize();
    cout << context << " " << p->GetSize() << endl;
}

//void ReceivedPacketIPLayer(std::string context, const Ptr< const Packet > p, const TcpHeader &header, const Ptr< const TcpSocketBase > socket)
void ReceivedPacketIPLayer(std::string context, const Ptr<const Packet> p,  Ptr< Ipv4 >
, uint32_t a)
{
    cout << "inside ip call back" << endl;
    //double curTime = Simulator :: GetSeconds();
    //m_bytesTotal[0] += p->GetSize();
     cout<<context<<" "<<p->GetSize ()<<endl;
    m_bytesTotal[0] += p->GetSize();
/*
    NewSnrTag tag;
    if (p->PeekPacketTag(tag))
    {
        cout << "Received Packet with SRN = " << tag.Get() << endl;
        double temp_snr = tag.Get();
    }
    else
    {
        cout << "peek return null" << endl;
    }
    */


//#endif 
}

void Throughput() // in Mbps calculated every 2s
{
    double mbps = (((m_bytesTotal[0] * 8.0f) / 1000000.0f) / (double)INTERVAL);
    double time_tp = Simulator::Now().GetSeconds();
    cout << "time: " << time_tp << "\t"
         << "NS3 total Throughput: " << mbps << "Mbps" << endl;
    m_bytesTotal[0] = 0;
    Simulator::Schedule(Seconds(INTERVAL), &Throughput);
}

///////////////////////////////////////////////

int main(int argc, char *argv[])
{
    //LogComponentEnable("MpTcpSocketBase", LOG_INFO);

    const char *netmask = "255.255.255.0";
    int nUE = 2;

    int stopTime = 10;

    std::ostringstream cmd_oss;

    CommandLine cmd;
    cmd.AddValue("nUE", "the number of UEs.", nUE);

    Config::SetDefault("ns3::Ipv4GlobalRouting::FlowEcmpRouting", BooleanValue(false));
    Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(1400));
    Config::SetDefault("ns3::TcpSocket::DelAckCount", UintegerValue(0));
    Config::SetDefault("ns3::DropTailQueue::Mode", StringValue("QUEUE_MODE_PACKETS"));
    Config::SetDefault("ns3::DropTailQueue::MaxPackets", UintegerValue(100));
    Config::SetDefault("ns3::TcpL4Protocol::SocketType", TypeIdValue(MpTcpSocketBase::GetTypeId()));
    // Config::SetDefault("ns3::MpTcpSocketBase::MaxSubflows", UintegerValue(8)); // Sink
    //Config::SetDefault("ns3::MpTcpSocketBase::CongestionControl", StringValue("RTT_Compensator"));
    //Config::SetDefault("ns3::MpTcpSocketBase::PathManagement", StringValue("NdiffPorts"));

    //define node and devices
    NodeContainer mn, cn;

    MobilityHelper mobility;
    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                  "MinX", DoubleValue(1.0),
                                  "MinY", DoubleValue(1.0),
                                  "DeltaX", DoubleValue(5.0),
                                  "DeltaY", DoubleValue(5.0),
                                  "GridWidth", UintegerValue(3),
                                  "LayoutType", StringValue("RowFirst"));
    mobility.SetMobilityModel("ns3::RandomWalk2dMobilityModel",
                              "Mode", StringValue("Time"),
                              "Time", StringValue("2s"),
                              //"Speed", StringValue("ns3::ConstantRandomVariable[Constant=1.0]"),
                              "Speed", StringValue("ns3::ConstantRandomVariable[Constant=0.0]"),
                              "Bounds", RectangleValue(Rectangle(0.0, 20.0, 0.0, 20.0)));

    mn.Create(nUE);
    cn.Create(nUE);

    mobility.Install(mn);

   for (int i=0; i< mn.GetN(); i++)
    {
        /*
          double angle = start+theta*i;
          double x = distance*cos(angle);
          double y = distance * sin(angle);
          //cout<<"Pos " <<i<<": "<<x << " "<< y<<" "<< x*x+y*y<<endl;*/
            setPosSpeed((mn.Get(i)), -20, 0 , 0, Vector(0, 0, 0) );
    }

    Ipv4StaticRoutingHelper ipv4routinghelper;

    InternetStackHelper internet;
    internet.Install(mn);
    internet.Install(cn);

    int routerI = mn.GetN();

    Ptr<OutputStreamWrapper> rtoutUE = Create<OutputStreamWrapper>("both-rtableUE", std::ios::out);
    Ptr<OutputStreamWrapper> rtoutCN = Create<OutputStreamWrapper>("both-rtableCN", std::ios::out);

    PointToPointHelper p2p;
    p2p.SetDeviceAttribute("DataRate", StringValue("100Mbps"));
    p2p.SetChannelAttribute("Delay", StringValue("10ms"));

#ifdef LTE
    Ipv4InterfaceContainer ifLte;
    Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();
    Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper>();

    NodeContainer enbNodes;
    enbNodes.Create(1);

    lteHelper->SetEpcHelper(epcHelper);
    Ptr<Node> pgw = epcHelper->GetPgwNode();

    //?? do we need to install
    //internet.Install(pgw);

    setPos(enbNodes.Get(0), 0, 0, 0);

    //lteHelper->SetSchedulerType ("ns3::PssFfMacScheduler");
    lteHelper->SetSchedulerType("ns3::PfFfMacScheduler");

    NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice(enbNodes);
    NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice(mn);

    // Assign ip addresses
    ifLte = epcHelper->AssignUeIpv4Address(NetDeviceContainer(ueLteDevs));

    for (unsigned int ii = 0; ii < ifLte.GetN(); ii++)
        cout << "ifLte  " << ii << "  " << ifLte.GetAddress(ii, 0) << endl;

    for (uint32_t u = 0; u < mn.GetN(); ++u)
    {
        lteHelper->Attach(ueLteDevs.Get(u), enbLteDevs.Get(0));
    }



    for (uint32_t u = 0; u < mn.GetN() - 1; ++u)
    {
        Ptr<Ipv4StaticRouting> mnStaticRouting = ipv4routinghelper.GetStaticRouting(mn.Get(u)->GetObject<Ipv4>());
        cout << "epc address " << epcHelper->GetUeDefaultGatewayAddress() << "  " << ueLteDevs.Get(u)->GetIfIndex() << endl;
        //mnStaticRouting ->AddNetworkRouteTo(if2Wifi.GetAddress (0, 0), Ipv4Mask ("255.255.255.0"), ifLte.GetAddress (routerI, 0), devices1.Get (u)->GetIfIndex ());
        mnStaticRouting->SetDefaultRoute(epcHelper->GetUeDefaultGatewayAddress(), ueLteDevs.Get(u)->GetIfIndex());
        //cout<<"devices1.Get (u)->GetIfIndex () : "<<devices1.Get (u)->GetIfIndex ()<<endl;
    }

    setPos(pgw, -2000, 0, 0);

    ////setting the other side

    for (int jj = 1; jj <= nUE; jj++){

        std::stringstream netAddr;
        netAddr << "10.2." << jj << ".0";

        NetDeviceContainer cont = p2p.Install(cn.Get(jj - 1), pgw);
        //p2p.EnablePcapAll(netAddr.str(), true);
        p2p.EnablePcap(string("lte-")+netAddr.str(), cont.Get(0));

        Ipv4AddressHelper ipv4;
            NS_LOG_DEBUG("setting ipv4 base " << netAddr.str());
        ipv4.SetBase(netAddr.str().c_str(), netmask);
        Ipv4InterfaceContainer ipInterfs = ipv4.Assign(cont);
        cout << ipInterfs.GetAddress(0, 0) << "  ";
        cout << ipInterfs.GetAddress(1, 0) << endl;

        Ptr<Ipv4StaticRouting> cnStaticRouting = ipv4routinghelper.GetStaticRouting(cn.Get(jj - 1)->GetObject<Ipv4>());

        cnStaticRouting->SetDefaultRoute(ipInterfs.GetAddress(1, 0), cont.Get(0)->GetIfIndex());
        //routerStaticRouting ->AddNetworkRouteTo(ipInterfs.GetAddress (0, 0), Ipv4Mask ("255.255.255.0"), ipInterfs.GetAddress (1, 0), cont.Get (1)->GetIfIndex ());
    }

#endif

#ifdef WIFI

    NetDeviceContainer wifiDevice;
    NetDeviceContainer devices1;
    NodeContainer WifiRouters;
    WifiRouters.Create(1);
    internet.Install(WifiRouters);

    setPos(WifiRouters.Get(0), 0, 0, 0);

    WifiHelper wifi = WifiHelper::Default();
    wifi.SetRemoteStationManager("ns3::AarfWifiManager");
    YansWifiPhyHelper phy = YansWifiPhyHelper::Default();
    YansWifiChannelHelper phyChannel = YansWifiChannelHelper::Default();
    NqosWifiMacHelper mac;
    phy.SetChannel(phyChannel.Create());
    //mac.SetType ("ns3::AdhocWifiMac");
    //
    int WifiAPI = 0;

    cmd_oss.str("");

    cmd_oss << "wifi-default-" << WifiAPI;
    Ssid ssid = Ssid(cmd_oss.str());

    mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid), "ActiveProbing", BooleanValue(false));

    wifi.SetStandard(WIFI_PHY_STANDARD_80211a);

    for (uint32_t u = 0; u < mn.GetN(); ++u)
    {
        wifiDevice = wifi.Install(phy, mac, mn.Get(u));
        devices1.Add(wifiDevice);
    }

    mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
    wifiDevice = wifi.Install(phy, mac, WifiRouters.Get(0));

    devices1.Add(wifiDevice);

    Ipv4AddressHelper address1;
    address1.SetBase("10.1.0.0", "255.255.255.0");
    Ipv4InterfaceContainer ifMnWiFi;
    // Assign ip addresses
    ifMnWiFi = address1.Assign(devices1);

    for (uint32_t u = 0; u < ifMnWiFi.GetN(); ++u)
    {
        cout << "ifMnWiFi: " << u << " " << ifMnWiFi.GetAddress(u, 0) << endl;
    }

        for (uint32_t u = 0; u < mn.GetN() - 1; ++u)
    {
        Ptr<Ipv4StaticRouting> mnStaticRouting = ipv4routinghelper.GetStaticRouting(mn.Get(u)->GetObject<Ipv4>());
        //mnStaticRouting ->AddNetworkRouteTo(if2Wifi.GetAddress (0, 0), Ipv4Mask ("255.255.255.0"), ifLte.GetAddress (routerI, 0), devices1.Get (u)->GetIfIndex ());
        mnStaticRouting->SetDefaultRoute(ifMnWiFi.GetAddress (routerI, 0),  devices1.Get (u)->GetIfIndex ());
        //mnStaticRouting->AddNetworkRouteTo(ifMnWiFi.GetAddress (routerI, 0), Ipv4Mask ("255.255.255.0"), devices1.Get (u)->GetIfIndex ());
        

        //cout<<"devices1.Get (u)->GetIfIndex () : "<<devices1.Get (u)->GetIfIndex ()<<endl;
    }

    for (int ii = 1; ii <= nUE; ii++)
    {

        std::stringstream netAddr;
        netAddr << "10.3." << ii << ".0";

        NetDeviceContainer cont = p2p.Install(cn.Get(ii - 1), WifiRouters.Get(0));
        //p2p.EnablePcapAll(netAddr.str(), true);
        p2p.EnablePcap(string("wifi-")+netAddr.str(), cont.Get(0));

        Ipv4AddressHelper ipv4;
        NS_LOG_DEBUG("setting ipv4 base " << netAddr.str());
        ipv4.SetBase(netAddr.str().c_str(), netmask);
        Ipv4InterfaceContainer ipInterfs = ipv4.Assign(cont);
        cout << ipInterfs.GetAddress(0, 0) << "  ";
        cout << ipInterfs.GetAddress(1, 0) << endl;

        Ptr<Ipv4StaticRouting> cnStaticRouting = ipv4routinghelper.GetStaticRouting(cn.Get(ii - 1)->GetObject<Ipv4>());

        //cnStaticRouting->AddNetworkRouteTo(ipInterfs.GetAddress(1, 0), Ipv4Mask("255.255.255.0"), cont.Get(0)->GetIfIndex());
        cnStaticRouting->SetDefaultRoute(ipInterfs.GetAddress(1, 0), cont.Get(0)->GetIfIndex());
        //routerStaticRouting ->AddNetworkRouteTo(ipInterfs.GetAddress (0, 0), Ipv4Mask ("255.255.255.0"), ipInterfs.GetAddress (1, 0), cont.Get (1)->GetIfIndex ());
    }

        phy.EnablePcapAll("mn-wifi", false);

#endif

    //  NetDeviceContainer devices;
    // devices = pointToPoint.Install(mn);

    //routerStaticRouting= ipv4routinghelper.GetStaticRouting (WifiRouters.Get(0)->GetObject<Ipv4>());
    //routerStaticRouting->AddNetworkRouteTo(ifLte.GetAddress (0, 0), Ipv4Mask ("255.255.255.0"), ifLte.GetAddress (routerI, 0), devices1.Get (routerI)->GetIfIndex ());

    // Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    Ptr<Ipv4StaticRouting> mnStaticRoutingForPrint = ipv4routinghelper.GetStaticRouting(mn.Get(0)->GetObject<Ipv4>());
    Ptr<Ipv4StaticRouting> cnStaticRoutingForPrint = ipv4routinghelper.GetStaticRouting(cn.Get(0)->GetObject<Ipv4>());

    mnStaticRoutingForPrint->PrintRoutingTable(rtoutUE);
    cnStaticRoutingForPrint->PrintRoutingTable(rtoutCN);


#ifdef WIFI

     Ptr<OutputStreamWrapper> rtoutRouter = Create<OutputStreamWrapper> ("both-rttableRouter", std::ios::out);
Ptr<Ipv4StaticRouting> routerStaticRoutingForPrint = ipv4routinghelper.GetStaticRouting (WifiRouters.Get(0)->GetObject<Ipv4>());
 routerStaticRoutingForPrint->PrintRoutingTable(rtoutRouter);
#endif

    uint16_t port = 9;

    for (int i = 0; i < nUE; i++)
    {

        //PacketSinkHelper sink = PacketSinkHelper ("ns3::TcpSocketFactory",InetSocketAddress(Ipv4Address::GetAny(), 9));

        MpTcpPacketSinkHelper sink("ns3::TcpSocketFactory", InetSocketAddress(Ipv4Address::GetAny(), port));
        ApplicationContainer sinkApps = sink.Install(mn.Get(i));
        sinkApps.Start(Seconds(0.1));
        sinkApps.Stop(Seconds(stopTime));
    }

    //////////////////////////////////////////////////////////////////////////
    //
    //   for(int ii=0; ii< mn.GetN(); ii++)
    // {
    //     char aa[10];
    //     sprintf( aa, "%d", ii);
    //     //std::string sink = "/NodeList/"+std::string(aa)+"/ApplicationList/*/$ns3::PacketSink/Rx";
    //     std::string sink = "/NodeList/"+std::string(aa)+"/ApplicationList/*/$ns3::PacketSink/Rx";
    //     //std::string sink = "/NodeList/[25-29]/ApplicationList/*/$ns3::PacketSink/Rx";
    //     Config::Connect (sink, MakeCallback(&ReceivedPacket) );
    // }

    std::string sink = "/NodeList/*/ApplicationList/*/$ns3::MpTcpPacketSink/Rx";
    Config::Connect(sink, MakeCallback(&ReceivedPacket));
    Simulator::Schedule(Seconds(INTERVAL / 2.0f), &Throughput);

    //////////////////////////////////////////////////////////////////////////
        char aa[10];
        sprintf(aa, "%d", mn.GetN());


        std::string sinkString = "/NodeList/[0-"+std::string(aa)+"]/$ns3::Ipv4L3Protocol/Rx";
        //std::string sinkString = "/NodeList/*/$ns3::Ipv4L3Protocol/Rx";
        //std::string sinkString = "/NodeList/[0-"+std::string(aa)+"]/$ns3::WifiMac/MacPromiscRx";
//Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac//MacPromiscRx", MakeCallback(&ReceivedPacketMacLayer));
//Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/MacPromiscRx", MakeCallback(&ReceivedPacketMacLayer));
//Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::StaWifiMac/MacRxDrop", MakeCallback(&ReceivedPacketMacLayer));
//MacDrop
//Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacRxDrop", MakeCallback(&MacDrop));
       //Config::Connect(sinkString, MakeCallback(&ReceivedPacketMacLayer));;
       Config::Connect(sinkString, MakeCallback(&ReceivedPacketIPLayer));


    for (int i = 0; i < nUE; i++)
    {
        cmd_oss.str("");
        cmd_oss << "10.1.0."<<i+1;
        //cmd_oss << "7.0.0." << i + 2;

        //MpTcpBulkSendHelper source("ns3::TcpSocketFactory", InetSocketAddress(Ipv4Address(i.GetAddress(1)), port));
        MpTcpBulkSendHelper source("ns3::TcpSocketFactory", InetSocketAddress(cmd_oss.str().c_str(), port));

        source.SetAttribute("MaxBytes", UintegerValue(0));
        ApplicationContainer sourceApps = source.Install(cn.Get(i));
        sourceApps.Start(Seconds(0.1));
        sourceApps.Stop(Seconds(stopTime));
    }

    NS_LOG_INFO("Run Simulation.");
    Simulator::Stop(Seconds(stopTime + 2));
    Simulator::Run();
    Simulator::Destroy();
    NS_LOG_INFO("Done.");
}
